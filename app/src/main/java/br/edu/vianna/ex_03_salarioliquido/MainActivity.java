package br.edu.vianna.ex_03_salarioliquido;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.material.textfield.TextInputEditText;

import br.edu.vianna.ex_03_salarioliquido.models.Salario;

public class MainActivity extends AppCompatActivity {
    private TextInputEditText txtHoras, txtSalarioHora, txtDependentes;
    private Button btnCalculaSalario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Salário Líquido");
        
        bindings();

        btnCalculaSalario.setOnClickListener(callCalculaSalario());
    }

    private View.OnClickListener callCalculaSalario() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Salario s = new Salario(
                        Double.parseDouble(txtHoras.getText().toString()),
                        Double.parseDouble(txtSalarioHora.getText().toString()),
                        Integer.parseInt(txtDependentes.getText().toString())
                );

                Intent i = new Intent(getApplicationContext(), SalarioActivity.class);
                i.putExtra("salario", s);

                startActivityForResult(i, 1);
            }
        };
    }

    private void bindings() {
        txtHoras = findViewById(R.id.txtHoras);
        txtSalarioHora = findViewById(R.id.txtSalarioHora);
        txtDependentes = findViewById(R.id.txtDependentes);

        btnCalculaSalario = findViewById(R.id.btnCalculaSalario);
    }
}