package br.edu.vianna.ex_03_salarioliquido;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import br.edu.vianna.ex_03_salarioliquido.models.Salario;

public class SalarioActivity extends AppCompatActivity {
    private Salario salario;
    private TextView txtSalarioLiquido;
    private Button btnVoltar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salario);

        salario = (Salario) getIntent().getExtras().getSerializable("salario");

        bindings();

        btnVoltar.setOnClickListener(callVoltar());

        calculaSalario(salario);
    }

    private void calculaSalario(Salario salario) {
        txtSalarioLiquido.setText(String.valueOf(salario.calculaSalarioLiquido()));
    }

    private View.OnClickListener callVoltar() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        };
    }

    private void bindings() {
        txtSalarioLiquido = findViewById(R.id.txtSalarioLiquido);
        btnVoltar = findViewById(R.id.btnBack);
    }
}