package br.edu.vianna.ex_03_salarioliquido.models;

import java.io.Serializable;

public class Salario implements Serializable {
    private double horas, salarioHora;
    private int dependentes;

    public Salario() {
    }

    public Salario(double horas, double salarioHora, int dependentes) {
        this.horas = horas;
        this.salarioHora = salarioHora;
        this.dependentes = dependentes;
    }

    public double getHoras() {
        return horas;
    }

    public void setHoras(double horas) {
        this.horas = horas;
    }

    public double getSalarioHora() {
        return salarioHora;
    }

    public void setSalarioHora(double salarioHora) {
        this.salarioHora = salarioHora;
    }

    public int getDependentes() {
        return dependentes;
    }

    public void setDependentes(int dependentes) {
        this.dependentes = dependentes;
    }

    public double calculaSalarioLiquido() {
        return calculaSalarioBruto() - descontoINSS() - descontoIR();
    }

    private double calculaSalarioBruto() {
        double salarioBruto;

        salarioBruto = horas * salarioHora + (50 * dependentes);

        return salarioBruto;
    }

    private double descontoINSS() {
        double salarioBruto = calculaSalarioBruto();
        if (salarioBruto <= 1000) {
            return salarioBruto * 8.5 / 100;
        } else {
            return salarioBruto * 9 / 100;
        }
    }

    private double descontoIR() {
        double salarioBruto = calculaSalarioBruto();

        if (salarioBruto <= 500) {
            return 0;
        } else if (salarioBruto > 500 && salarioBruto <= 1000) {
            return salarioBruto * 5 / 100;
        } else {
            return salarioBruto * 7 / 100;
        }
    }
}
